       IDENTIFICATION DIVISION.
       PROGRAM-ID. BSTRING.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  BOOLEAN-BLANK     PIC X.
           88 IS-BLANK       VALUE "T".
           88 NOT-BLANK      VALUE "F".
       01  s                 PIC X(5) VALUE SPACES.
       PROCEDURE DIVISION.
           IF s = SPACES
              SET IS-BLANK     TO TRUE
           ELSE
              SET NOT-BLANK    TO TRUE
           END-IF.
           DISPLAY BOOLEAN-BLANK	 	
           GOBACK.